This is just a stub README for the Quarren CMS.  More later!

* Installation

** Debian

You'll need the postgresql and libpq-dev packages installed, along with carton and cpanminus:

```
$ sudo apt-get install postgresql libpq-dev cpanminus

# Add a db
sudo -u postgres psql
postgres=# create database mydb;
postgres=# create user myuser with encrypted password 'mypass';
postgres=# grant all privileges on database mydb to myuser;

quarren    #  will prompt for homedir and DB connect info.
quarren-theme   # to install a theme
