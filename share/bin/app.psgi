#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;

$ENV{DANCER_CONFDIR} = "$FindBin::Bin/../";

require Quarren;
Quarren->to_app;
