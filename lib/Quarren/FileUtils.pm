package Quarren::FileUtils;

our $VERSION = '1.0';

use strict;
use warnings;

use Dancer2::Template::Simple;
use File::Find;
use File::Path 'mkpath';
use File::Share;
use File::Spec::Functions;
use File::Basename qw/dirname basename fileparse/;
use File::Copy;

sub build_file_list {
   my ( $from, $to ) = @_;
   $from =~ s{/+$}{};
   my $len = length($from) + 1;

   my @result;
   my $wanted = sub {
      return unless -f;
      my $file = substr( $_, $len );

      # ignore .git and git/*
      my $is_git = $file =~ m{^\.git(/|$)}
        and return;

      push @result, [ $_, catfile( $to, $file ) ];
   };

   find( { wanted => $wanted, no_chdir => 1 }, $from );
   return \@result;
}

sub copy_templates {
   my ( $files, $vars ) = @_;

   foreach my $pair (@$files) {
      my ( $from, $to ) = @{$pair};

      my $to_dir = dirname($to);
      if ( !-d $to_dir ) {
         print "+ $to_dir\n";
         mkpath $to_dir or die "could not mkpath $to_dir: $!";
      }

      my $to_file = basename($to);
      my $ex      = ( $to_file =~ s/^\+// );
      $to = catfile( $to_dir, $to_file ) if $ex;

      print "+ $to\n";
      my $content;

      {
         local $/;
         open( my $fh, '<:raw', $from )
           or die "unable to open file `$from' for reading: $!";
         $content = <$fh>;
         close $fh;
      }

      if ( $from !~ m/\.(ico|jpg|png|css|eot|map|swp|ttf|svg|woff|woff2|js)$/ )
      {
         $content = _process_template( $content, $vars );
      }

      open( my $fh, '>:raw', $to )
        or die "unable to open file `$to' for writing: $!";
      print $fh $content;
      close $fh;

      if ($ex) {
         chmod( 0755, $to ) or warn "unable to change permissions for $to: $!";
      }
   }
}

sub _process_template {
   my ( $template, $tokens ) = @_;
   my $engine = Dancer2::Template::Simple->new;
   $engine->{start_tag} = '[QB%';
   $engine->{stop_tag}  = '%BQ]';
   return $engine->render( \$template, $tokens );
}

1;
