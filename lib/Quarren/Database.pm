package Quarren::Database;

use strict;
use warnings;

use Dancer2;
use Dancer2::Plugin::DBIC;
use DBIx::Class::Schema::PopulateMore;

sub deploy_or_update_database {
   if ( !schema->get_db_version() ) {

      # brand new shiny install!
      schema->deploy();
      register_plugins();
      install_default_data();
   }
   if ( schema->schema_version ne schema->get_db_version ) {

      # we are behind the ball on the schema
      schema->upgrade();
   }
   return;
}

sub install_default_data {
   schema->populate_more(
      Role => {
         fields => 'role',
         data   => {
            owner => 'Owner',
         }
      },
      PageStatus => {
         fields => 'status',
         data   => {
            draft     => 'Draft',
            published => 'Published',
            trash     => 'Trash',
         }
      },
      SystemParameter => {
         fields => [ 'name', 'value_text', 'value_integer' ],
         data   => {
            default_page => [ 'default_page', undef,  undef ],
            permalink    => [ 'permalink',    'slug', undef ],
         }
      },
   );
   my $theme_class = config->{Quarren}->{theme};
   eval "require $theme_class";
   my $obj = $theme_class->new;
   $obj->install_default_data;
}

sub register_plugins {
   use Plugin::Simple;

   my @available_plugins = plugins('Quarren::Plugin::');
   foreach my $plugin (@available_plugins) {
      my ( $ns, $remain ) =
        $plugin =~ /Quarren\:\:Plugin\:\:([A-Za-z]+)\:\:(.*)/;
      next if !$ns || !$remain;
      my $plugin_rec = resultset('Plugin')
        ->find_or_new( { namespace => $ns, name => $remain } );
      next
        if
        $plugin_rec->in_storage; # it's already registered, so don't disturb it.
      $plugin_rec->active(1) if ( $ns eq 'PageRender' );
      $plugin_rec->insert;
   }
}

1;
