package Quarren::Bootstrap;

our $VERSION = '1.0';

use strict;
use warnings;

use Quarren::FileUtils;
use File::Share 'dist_dir';

sub copy_files_from_skel {
   my $params = shift;
   print "Creating a new Quarren instance at " . $params->{directory} . "...\n";
   if ( $params->{directory} !~ /^\// ) {
      $params->{directory} = $params->{cwd} . '/' . $params->{directory};
   }
   print "Actual directory specification: " . $params->{directory} . "\n"
     if $params->{verbose};

   # With a nod to Dancer2::CLI::Command::gen, copy the skeleton app into place
   #
   my $files_to_copy =
     Quarren::FileUtils::build_file_list( dist_dir('Quarren::Bootstrap'),
      $params->{directory} );

   Quarren::FileUtils::copy_templates( $files_to_copy, $params );

   return 1;
}

1;
