package Quarren::Schema::ResultSet;

use strict;
use warnings;

use parent 'DBIx::Class::ResultSet';

sub as_hash {
   my ($self) = @_;
   $self->result_class('DBIx::Class::ResultClass::HashRefInflator');
   return $self;
}

1;
