## Please see file perltidy.ERR
package Quarren::Schema::Result::LoginRole;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('login_role');

__PACKAGE__->add_columns(
   'id',
   {
      data_type         => 'integer',
      is_nullable       => 0,
      is_auto_increment => 1,
   },
   'login',
   {
      data_type      => 'integer',
      is_nullable    => 0,
      is_foreign_key => 1,
   },
   'role',
   {
      data_type      => 'integer',
      is_nullable    => 0,
      is_foreign_key => 1,
   },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
   "login",
   "Quarren::Schema::Result::Login",
   { id            => "login" },
   { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

__PACKAGE__->belongs_to(
   "role",
   "Quarren::Schema::Result::Role",
   { id            => "role" },
   { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

1;
