package Quarren::Schema::Result::SystemParameter;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('system_parameter');

__PACKAGE__->add_columns(
   'id',
   {
      data_type         => 'integer',
      is_nullable       => 0,
      is_auto_increment => 1,
   },
   'name',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'value_text',
   {
      data_type   => 'text',
      is_nullable => 1,
   },
   'value_dt',
   {
      data_type   => 'timestamp',
      is_nullable => 1,
   },
   'value_date',
   {
      data_type   => 'date',
      is_nullable => 1,
   },
   'value_integer',
   {
      data_type   => 'integer',
      is_nullable => 1,
   },
   'value_numeric',
   {
      data_type   => 'numeric',
      is_nullable => 1,
   }
);

__PACKAGE__->set_primary_key('id');

1;
