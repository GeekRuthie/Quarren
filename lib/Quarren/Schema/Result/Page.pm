package Quarren::Schema::Result::Page;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('page');

__PACKAGE__->add_columns(
   'id',
   {
      data_type         => 'integer',
      is_nullable       => 0,
      is_auto_increment => 1,
   },
   'creator',
   {
      data_type      => 'integer',
      is_nullable    => 0,
      is_foreign_key => 1,
   },
   'page_status',
   {
      data_type      => 'integer',
      is_nullable    => 0,
      is_foreign_key => 1,
   },
   'render_plugin',
   {
      data_type      => 'integer',
      is_nullable    => 0,
      is_foreign_key => 1,
   },
   'title',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'slug',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'layout',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'template',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'content',
   {
      data_type   => 'text',
      is_nullable => 1
   },
   'publication_dt',
   {
      data_type   => 'datetime',
      is_nullable => 1,
   },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
   "login",
   "Quarren::Schema::Result::Login",
   { id            => "creator" },
   { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

__PACKAGE__->belongs_to(
   "page_status",
   "Quarren::Schema::Result::PageStatus",
   { id            => "page_status" },
   { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

__PACKAGE__->belongs_to(
   "render_plugin",
   "Quarren::Schema::Result::Plugin",
   { id            => "render_plugin" },
   { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

__PACKAGE__->add_unique_constraint( [qw/slug/] );

1;
