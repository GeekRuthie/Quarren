package Quarren::Schema::Result::Session;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('session');

__PACKAGE__->add_columns(
   "id",
   {
      data_type   => "char",
      is_nullable => 0,
      size        => 40
   },
   "session_data",
   {
      data_type   => "text",
      is_nullable => 1
   },
   "created_dt",
   {
      data_type     => "timestamp",
      default_value => \"current_timestamp",
      is_nullable   => 1,
      original      => { default_value => \"now()" },
   },
   "modified_dt",
   {
      data_type   => "timestamp",
      is_nullable => 1
   },
);

__PACKAGE__->set_primary_key('id');

1;
