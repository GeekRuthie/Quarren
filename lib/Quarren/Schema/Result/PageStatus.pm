package Quarren::Schema::Result::PageStatus;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('page_status');

__PACKAGE__->add_columns(
   'id',
   {
      data_type         => 'integer',
      is_nullable       => 0,
      is_auto_increment => 1,
   },
   'status',
   {
      data_type   => 'text',
      is_nullable => 0
   },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
   "pages",
   "Quarren::Schema::Result::Page",
   { 'foreign.page_status' => 'self.id' },
);

1;
