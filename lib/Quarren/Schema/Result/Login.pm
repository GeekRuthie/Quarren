package Quarren::Schema::Result::Login;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('login');

__PACKAGE__->add_columns(
   'id',
   {
      data_type         => 'integer',
      is_nullable       => 0,
      is_auto_increment => 1,
   },
   'login',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'password',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'last_login_dt',
   {
      data_type   => 'timestamp',
      is_nullable => 1
   },
   'pw_reset_code',
   {
      data_type   => 'text',
      is_nullable => 1
   },
   'pw_changed_dt',
   {
      data_type   => 'timestamp',
      is_nullable => 1
   },
   'name',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'email',
   {
      data_type   => 'text',
      is_nullable => 1
   },
   'slug',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'active',
   {
      data_type     => 'boolean',
      default_value => \"true",
      is_nullable   => 0
   },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
   "login_roles",
   "Quarren::Schema::Result::LoginRole",
   { "foreign.login" => "self.id" },
   { cascade_copy    => 0, cascade_delete => 0 },
);

__PACKAGE__->many_to_many(
   roles => 'login_roles',
   'role'
);

__PACKAGE__->has_many(
   "pages",
   "Quarren::Schema::Result::Page",
   { 'foreign.creator' => 'self.id' },
);

__PACKAGE__->add_unique_constraint( [qw/slug/] );
__PACKAGE__->add_unique_constraint( [qw/login/] );

1;
