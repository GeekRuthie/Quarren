package Quarren::Schema::Result::Role;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('role');

__PACKAGE__->add_columns(
   'id',
   {
      data_type         => 'integer',
      is_nullable       => 0,
      is_auto_increment => 1,
   },
   'role',
   {
      data_type   => 'text',
      is_nullable => 0
   },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
   "login_roles",
   "Quarren::Schema::Result::LoginRole",
   { "foreign.role" => "self.id" },
   { cascade_copy   => 0, cascade_delete => 0 },
);

__PACKAGE__->many_to_many( 'logins' => 'login_roles', 'login' );

1;
