package Quarren::Schema::Result::Plugin;

use strict;
use warnings;

use base 'Quarren::Schema::Result';

__PACKAGE__->table('plugin');

__PACKAGE__->add_columns(
   'id',
   {
      data_type         => 'integer',
      is_nullable       => 0,
      is_auto_increment => 1,
   },
   'namespace',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'name',
   {
      data_type   => 'text',
      is_nullable => 0
   },
   'active',
   {
      data_type     => 'boolean',
      default_value => \"false",
      is_nullable   => 0
   },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
   "pages",
   "Quarren::Schema::Result::Page",
   { 'foreign.render_plugin' => 'self.id' },
);

1;
