package Quarren::Schema::Upgrade;

use strict;
use warnings;

use base 'DBIx::Class::Schema::Versioned::Inline::Upgrade';
use DBIx::Class::Schema::Versioned::Inline::Upgrade qw/before after/;
use DBIx::Class::Schema::PopulateMore;

1;
