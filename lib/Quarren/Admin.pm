package Quarren::Admin;

use strict;
use warnings;

use Dancer2 appname => 'Quarren';
use Dancer2::Plugin::Auth::Extensible;
use Dancer2::Plugin::DBIC;

prefix '/admin';

get '' => require_login sub {
   return template
     'admin/dashboard' => {},
     { layout => 'admin' };
};

1;
