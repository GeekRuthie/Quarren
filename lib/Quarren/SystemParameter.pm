package Quarren::SystemParameter;

use strict;
use warnings;

use Dancer2 appname => 'Quarren';
use Dancer2::Plugin::Cache::CHI;
use Quarren::Schema;

sub get_param {
   my ($param) = @_;
   my $cache = cache_get 'system_parameters';
   return $cache->{$param};
}

1;
