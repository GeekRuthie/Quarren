package Quarren::Schema;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

our $FIRST_VERSION = '0.001';
our $VERSION       = '0.001';

__PACKAGE__->load_namespaces;
__PACKAGE__->load_components(
   qw/Schema::PopulateMore Schema::ResultSetNames Schema::Versioned::Inline/);
Quarren::Schema->load_namespaces( default_resultset_class => 'ResultSet' );

1;
