package Quarren::Permalink;

use strict;
use warnings;

use Dancer2 appname => 'Quarren';
use Dancer2::Plugin::DBIC;

sub slug {
   my ( undef, $route ) = @_;
   return undef if scalar @{$route} != 1;
   return resultset('Page')
     ->find( { slug => $route->[0] }, { prefetch => ['render_plugin'] } );
}

#sub collection_slug {
#    my (undef,$route) = @_;
# return undef if scalar @{$route} != 2;
# return resultset('Page')->find(
#     { slug => $route->[1], 'page_collection.slug' => $route->[0] },
#  { join => ['page_collection'], prefetch => ['render_plugin'] }
#);
#}

1;
