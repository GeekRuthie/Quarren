package Quarren::Plugin::PageRender;

use strict;
use warnings;

use Quarren::FileUtils;
use File::Share 'dist_dir';

sub new {
   my $class = shift;
   my $self  = {@_};
   return bless $self, $class;
}

sub render_content {
   return undef;
}

1;
