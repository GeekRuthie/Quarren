package Quarren::Plugin::PageRender::AsIs;

use strict;
use warnings;

use parent 'Quarren::Plugin::PageRender';

sub new {
   my $class = shift;
   my $self  = {@_};
   return bless $self, $class;
}

sub render {
   my ( $self, $content ) = @_;
   return $content;
}

1;
