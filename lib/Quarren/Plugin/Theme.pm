## Please see file perltidy.ERR
package Quarren::Plugin::Theme;

use strict;
use warnings;

use Quarren::FileUtils;
use File::Share 'dist_dir';

sub new {
   my $class = shift;
   my $self  = {@_};
   return bless $self, $class;
}

sub layouts_available {
   return undef;
}

sub singles_available {
   return undef;
}

sub lists_available {
   return undef;
}

sub enumerations_available {
   return undef;
}

sub deploy {
   my ( $self, $target_dir ) = @_;

   my $files_to_copy =
     Quarren::FileUtils::build_file_list( dist_dir(caller), $target_dir );
   Quarren::FileUtils::copy_templates( $files_to_copy, $target_dir );

   return 1;
}

sub install_initial_data {
   return undef;
}

1;
