package Quarren::Cache;

use strict;
use warnings;

use Dancer2 appname => 'Quarren';
use Dancer2::Plugin::Cache::CHI;
use Dancer2::Plugin::DBIC;
use Plugin::Simple;
use Quarren::Schema;

sub cache_plugins {
   my @plugins =
     resultset('Plugin')->search( { namespace => 'PageRender' } )->all();
   foreach my $plugin (@plugins) {
      my $shortname   = $plugin->namespace . '::' . $plugin->name;
      my $full_plugin = plugins( 'Quarren::Plugin::' . $shortname );
      my $plugin_obj;
      $plugin_obj = $full_plugin->new();
      cache_set $shortname => $plugin_obj;
   }
   return undef;
}

sub cache_system_parameters {
   my @parameters = resultset('SystemParameter')->all();
   my $system_parameters;
   foreach my $param (@parameters) {
      foreach my $field (
         qw/value_numeric value_text value_date value_integer value_dt/)
      {
         $system_parameters->{ $param->name }->{$field} = $param->$field;
      }
   }
   cache_set system_parameters => $system_parameters;
   return undef;
}

1;
