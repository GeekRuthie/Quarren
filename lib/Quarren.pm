package Quarren;

# ABSTRACT: A Dancer2 CMS with DBIx::Class and Text::Handlebars
use strict;
use warnings;
use Dancer2;
use Dancer2::Plugin::Auth::Extensible;
use Dancer2::Plugin::Cache::CHI;
use Dancer2::Plugin::DBIC;
use Dancer2::Session::DBIC;
use Dancer2::Template::Handlebars;

use Quarren::Admin;
use Quarren::Cache;
use Quarren::Database;
use Quarren::Permalink;
use Quarren::SystemParameter;

BEGIN {
   Quarren::Database::deploy_or_update_database();
   Quarren::Cache::cache_system_parameters();
   Quarren::Cache::cache_plugins();
}

check_page_cache;

prefix undef;

#
# These routes handle all of the public-facing pages. Yes, that's really all there
# is to it.
#

get '/' => sub {
   my $page = vars->{page};
   if ( !defined $page ) {
      my $param = Quarren::SystemParameter::get_param('default_page');
      if ( defined $param->{value_integer} ) {
         $page = resultset('Page')
           ->find( $param->{value_integer}, { prefetch => ['render_plugin'] } );
      } else {
         $page =
           resultset('Page')
           ->search( {}, { order_by => 'id', prefetch => ['render_plugin'] } )
           ->first();
      }
   }
   send_error( "", 404 ) if !defined $page;
   my $render_plugin = cache_get 'PageRender::' . $page->render_plugin->name;
   my $page_content  = $render_plugin->render( $page->content );
   return cache_page template $page->template => { content => $page_content },
     { layout => $page->layout };
};

get '/**' => sub {
   my ($route) = splat;
   my $page = vars->{page};
   if ( !defined $page ) {
      my $permalink =
        Quarren::SystemParameter::get_param('permalink')->{value_text};
      $page = Quarren::Permalink->$permalink($route);
   }
   send_error( "", 404 ) if !defined $page;
   my $render_plugin = cache_get 'PageRender::' . $page->render_plugin->name;
   my $page_content  = $render_plugin->render( $page->content );
   return cache_page template $page->template => { content => $page_content },
     { layout => $page->layout };
};

1;

__END__

=head1 SYNOPSIS

Quarren is a nerdy, pragmatic CMS written with L<Dancer2>, L<DBIx::Class>,
and L<Text::Handlebars>.

=head1 WHAT'S A QUARREN?

Quarren was initially written during the COVID-19 Epidemic of 2020. The
original author never contracted the disease, but practiced social distancing
very aggressively, to protect her husband, who was at high risk. She wasn't
in quarantine, as such -- but spending time writing this CMS put her in
"Quarren-tine".

=head1 WHENCE QUARREN?

WordPress is amazing, in many, many ways; it's ubiquitous, easy-to-install,
understandable, and (relatively) easy to hack on, if you can bring yourself to
work on PHP code. But there are many things that would be really nice to have
that aren't there, which, over time, have become a part of the paid-plugin
ecosystem around WordPress.

We're not knocking making money off of software -- let's face it, most of
the people who are reading this do precisely that. But it's frustrating to
not be able to, for instance, find a tool to load a bunch of posts via a
CSV that doesn't involve a bunch of ads to Buy! Buy! Buy! and getting on
spammy email lists.

There are other feature-rich CMSes out there, of course. But for most of
them, for many users, you start to lose out on the qualities that make
WordPress so wonderful: hard-to-install, poorly-documented, so complicated
that a tax attorney can't figure it out, and/or impossible to modify to
suit purposes without a B<lot> of patience and time.

On the flip side of the equation, systems like Jekyll and Hugo, while quite
flexible and (frankly) wonderful, do not do interactive sites well. To do 
searching at all, you have to bolt on additional complexity through some 
external product, and keep it synced up with the site, and so forth.  In
particular, faceted searching Just Is Not There.

And thus, Quarren: A whole raft of useful features, a pragmatic, flexible
data storage schema, a mindset toward making installation of common use-
cases relatively simple, and a straightforward plugin system that will
let clever folks extend it in new and interesting ways.

=head1 WHAT QUARREN IS NOT

It's not WordPress. There isn't a massive team of UI engineers working on the
blog-owner-facing "admin" section of Quarren, so WYSIWYG editing, drag-and-
drop-everything, and graphical block editing a la Gutenberg will probably
never happen. But if you can hack some HTML/Handlebars, and want a system
where you can upload POD, Markdown, raw HTML/JS, or some CSVs to create
content, maybe Quarren is the CMS for you.

=head1 WHITHER QUARREN?

It would be nice if other people decided to hack on Quarren, with the following
goals:

=over 4

=item *

More themes!

=item *

More search engines!

=item *

More database independence!

=item *

More renderers for different types of documents!

=item *

More shortcode plugins!

=back

=head1 DOCUMENTATION

Most folks will only need these documents to get started enjoying Quarren:

=over 4

=item *

L<Quarren::Manual::Installation> - Installing Quarren and Quarren plugins

=item *

L<Quarren::Manual::Administration> - The user manual for site owners running Quarren

=back 

...but to join the project and contribute, you'll want to take a look at these:

=over 4

=item *

L<Quarren::Manual::Policy> - Project policies and code of conduct

=item *

L<Quarren::Manual::Schema> - A more-thorough description of the Quarren database schema

=item *

L<Quarren::Manual::Plugin::Cron> - How to construct a Quarren cron job plugin

=item *

L<Quarren::Manual::Plugin::PageRender> - How to construct a Quarren page renderer plugin

=item *

L<Quarren::Manual::Plugin::Search> - How to construct a Quarren search plugin

=item *

L<Quarren::Manual::Plugin::Shortcode> - How to construct a Quarren shortcode plugin

=item *

L<Quarren::Manual::Plugin::Theme> - How to construct a Quarren theme

=item *

L<Quarren::Manual::Plugin::UploadRender> - How to construct a Quarren upload renderer plugin

=back

=cut
